include "rogue/game system data/fruit/skillsystem/metacategories.fruit"
include "rogue/game system data/fruit/ai/npctypes.fruit"
include "rogue/game system data/fruit/weapon/weapon.fruit"

class SkillCondition
{
    string myConditionType ""
	bool myIsNegated FALSE
}

list ConditionInternalList
{
	SkillCondition
}

enum ConditionOperator
{
	AND
	OR
}

class ConditionList
{
	ConditionOperator myOperator AND
	ConditionInternalList myConditionList
}

class SCTargetIsFlanked : SkillCondition
{
    string myConditionType "TargetIsFlanked"
}

class SCIsInCombat : SkillCondition
{
    string myConditionType "IsInCombat"
}

class SCIsStunned : SkillCondition
{
    string myConditionType "IsStunned"
}

class SCTargetIsStunned : SkillCondition
{
    string myConditionType "TargetIsStunned"
}

class SCIsInCover : SkillCondition
{
	string myConditionType "IsInCover"
}

class SCTargetIsInCover : SkillCondition
{
	string myConditionType "TargetIsInCover"
}

class SCIsAiming : SkillCondition
{
	string myConditionType "IsAiming"
}

class SCIsTargetAiming : SkillCondition
{
	string myConditionType "IsTargetAiming"
}


class SCHealthBase < abstract=1 > : SkillCondition 
{
	float myHealthPercentageGreaterThanThreshold 0.5
}

class SCHealth : SCHealthBase
{
	string myConditionType "Health"
}

class SCTargetHealth : SCHealthBase
{
	string myConditionType "TargetHealth"
}

class SCCoverHealth : SCHealthBase
{
	string myConditionType "CoverHealth"
}

class SCTargetCoverHealth : SCHealthBase
{
	string myConditionType "TargetCoverHealth"
}

class SCTargetIsAlive : SkillCondition
{
	string myConditionType "TargetIsAlive"
}

class SCHasActiveMission : SkillCondition
{
	string myConditionType "HasActiveMission"
	string myActiveMission ""
}

class SCHasActiveMissionObjective : SCHasActiveMission
{
	string myConditionType "HasActiveMissionObjective"
	int myMissionObjective 0
}

class SCHasCompletedMission : SkillCondition
{
	string myConditionType "HasCompletedMission"
	string myCompletedMission ""
}

class SCIsPlayerController : SkillCondition
{
	string myConditionType "IsPlayerController"
}

class SCTargetIsSelf : SkillCondition
{
	string myConditionType "TargetIsSelf"
}

class SCHasAuraCountBase < abstract=1 > : SkillCondition 
{
	string myAura ""
	int myCount -1 < tooltip="-1 to ignore stack count" >
}

class SCHasAuraCount : SCHasAuraCountBase
{
	string myConditionType "HasAuraCount"
}

class SCTargetHasAuraCount : SCHasAuraCountBase
{
	string myConditionType "TargetHasAuraCount"
}

class SCHasMetaCategoryBase < abstract=1 > : SkillCondition 
{
	MetaCategory myMetaCategory 
}

class SCHasMetaCategory : SCHasMetaCategoryBase
{
	string myConditionType "HasMetaCategory"
}

class SCTargetHasMetaCategory : SCHasMetaCategoryBase
{
	string myConditionType "TargetHasMetaCategory"
}

class SCTargetIsHostile : SkillCondition
{
	string myConditionType "TargetIsHostile"
}

class SCIsInsideVolume : SkillCondition
{
	string myConditionType "IsInsideVolume"
	string myVolume ""
}

class SCIsSkillTriggerInRange : SkillCondition
{
	string myConditionType "IsSkillTriggerInRange"
	MetaCategoryList myMetaCategoryList
	float myRange
}

class SCTargetIsLinked : SkillCondition
{
	string myConditionType "TargetIsLinked"
}

class SCTargetIsMarked : SkillCondition
{
	string myConditionType "TargetIsMarked"
}

class SCUserHasLinksOrMarks : SkillCondition
{
	string myConditionType "UserHasLinksOrMarks"
}

class SCCanTeleport : SkillCondition
{
	string myConditionType "CanTeleport"
}

class SCTargetIsNPCOfType : SkillCondition
{
	string myConditionType "TargetIsNPCOfType"
	NPCArchetype myArchetype Unset
}

class SCHasLos : SkillCondition
{
	string myConditionType "HasLos"
}

class SCWeaponGroup : SkillCondition
{
	string myConditionType "UseWeaponGroup"
	WeaponGroups myWeaponGroup
}

class SCIsOwnerInvisible : SkillCondition
{
	string myConditionType "IsOwnerInvisible"
}