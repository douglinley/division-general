include "rogue/game system data/fruit/item/items.fruit"
include "rogue/game system data/fruit/attribute/attribute_progression.fruit"
include "rogue/game system data/fruit/skillsystem/covertocoverskill.fruit"
include "rogue/game system data/fruit/loot/loottables.fruit"
include "rogue/game system data/fruit/ai/detectionsoundtypes.fruit"

list SkillRefList
{
	Skill*
}

class SkillUnlockList
{
	int myLevel
	SkillRefList mySkillList
}

list LevelSkillUnlockList
{
	SkillUnlockList
}

list DefaultEquipments
{
	string
}

class DefaultEquipmentItem
{
	DefaultEquipments myItems
	int myStartingStackCount 1
}

list DefaultEquipmentList
{
	DefaultEquipmentItem
}

list HighLevelTestGear
{
	string
}

class AttachedEffect
{
	string myEffectName
	float myRadius
}

list AttachedEffectList
{
	AttachedEffect
}

enum ResourceType
{
	CONCENTRATION
	AGGRESSION
}

enum RegenState
{
	DEFAULT_REGEN
	IN_COMBAT_REGEN
}

enum PlayerControllerType
{
	PlayerCharacter
    PlayerFreeFlight
	PlayerVectoredFlight
}

class Segmentation
{
	bool myHealthCanFullyRegen TRUE
	bool myHealthCanFullyRegenInCombat TRUE
	int myNumSprintSegment 1
}

class HPSafetyBelt < tooltip="Cooldown is set by the attribute SafetyBeltCooldownFinal in the attribute dictionary. Setting that attribute to be less than 0 will also disable this mechanic." >
{
	float myNeededHealth
	float myResultingHealth
	float myResultingHealthRnd
	int myLevelRange
	float myImmunitySeconds
	float myTimeSpanSeconds
}

class Health
{
	Segmentation mySegmentation
	HPSafetyBelt myHPSafetyBelt
}

class RespawnParameters
{
	float myMinAmmo 0.0
	float myMinMedkits 0.0
	float myMinGrenades 0.0
}

class TargetPoint
{
	string myTargetPointName ""
	string myTargetPointNPC ""
	bool myIsTargetable TRUE
	bool myPassDamageToAgent TRUE
	float myMinAttackAngle 0.0
	float myMaxAttackAngle 3.14159
	vec3f myOffset
}

list TargetPointList
{
	TargetPoint
}

class PlayerClass < uid=auto >
{
	string myUIName "Unknown class"
	color myClassColor 0xffa0ffa0
	file myIcon "rogue/ui/ClassIcons/UI-ClassIconDefault-01.tga"
	file myClassBannerImage ""
	file myClassBannerImage_MouseOver ""
	file myClassBannerImage_Selected ""
	locstring myClassInfoBoxText ""

	file myGraphObject ""
	file myDefaultHeadGraphObject ""
	file myDefaultChestGraphObject ""
	file myDefaultLegsGraphObject ""
	file myDefaultShoesGraphObject ""
	file myDefaultGlovesGraphObject ""
	
	bool myUseSessionConstantGraphObject TRUE
	bool myUseAdditionalBodyParts TRUE
	
	LootTable* myLootTable
	AttributeProgression* myAttributeProgression

	DefaultEquipmentList myDefaultEquipmentList
	HighLevelTestGear myHighLevelTestGear
	
	float myApplyAuraOverResourceLimit -1.0
	string myOverResourceLimitAura ""
	
	float myApplyAuraBelowResourceLimit -1.0
	string myBelowResourceLimitAura ""

	//Player specific
	AttachedEffectList myAttachedEffects
	SkillRefList myMeleeSkills
	SkillRefList myCoreSkills	
	SkillRefList mySkills
	LevelSkillUnlockList mySkillUnlocks
	CoverToCoverSkillParams myCoverToCoverSkillParams
	file myTalentTreeFile ""
	file myMenuSkillScripts ""
	file myMenuEmotes ""
	
	DetectionSoundType* myWalkingDetectionSoundType
	DetectionSoundType* myParkourDetectionSoundType
	float myNoiseCooldown 1.0
	
	//Healing modifiers
	bool myUseMeleeRangeHealingEfficiencyModifier FALSE
	float myMeleeRangeHealingEfficiencyMaxRange 0.0
	float myMeleeRangeHealingEfficiencyModifier 0.0

	Health myHealth
	RespawnParameters myRespawnParameters

	//Movies
	file myMoviePath ""
	
	//Sounds
	AudioSwitch myClassAudioSwitch
	AudioEvent myDeathAudioEvent
	AudioEvent myRespawnAudioEvent
	
	// Feedback
	bool myDisplayKillFeedback TRUE
	bool myAwardsXpOnKill TRUE
	
	// Auto Attack
	string myAutoAttackSkillToUse ""
	
	// Controller and Movement
	PlayerControllerType myPlayerControllerType
	float myBaseMoveSpeed 3.4
	
	// Covers
	bool  myCanPerformCoverToCoverMove TRUE
	float myCoverToCoverMaxRange 25
	float myCoverToCoverSpeedFactor 1
	float myHorizontalCoverSwapMinRange 0.21
	float myHorizontalCoverSwapMaxRange 10
	float myHorizontalCoverSwapMaxAngle 15
	
	// Targetting
	TargetPointList myTargetPoints
	
	// Sensor / Reaction
	file mySensorProfile ""
	
	// Custom Shaders
	file myPulseShader "rogue/shaders/special/pulse.mshader"
	file myTargetHighlightShaderOverride ""
	file myPulseHighlightShaderOverride ""
	file myChloeHighlightShaderOverride ""
	file myUIHighlightShaderOverride ""

	// Hitbox
	file myHitbox "rogue/game system data/juice/agentcollision/playerHitboxStates.juice"
	
	// Footsteps
	file myLeftFootstepGraphObject "rogue/graph objects/prop/ShoePrint-SnowLeft.mgraphobject"
	file myRightFootstepGraphObject "rogue/graph objects/prop/ShoePrint-SnowRight.mgraphobject"
	
	// Starting level for new profiles
	int myStartingLevel 1
}
