Rogue.Wallet.Add Default_Currency 1000
Rogue.Wallet.Add Quarantine_Currency 1000
Rogue.Wallet.Add Hard_Currency 1000
Rogue.Wallet.Add Intel_Currency 1000
Rogue.Wallet.Add Personnel_Currency 1000
Rogue.Wallet.Add DarkZoneKey_Access 1000
Rogue.Wallet.Add LockPick_Access 1000
Rogue.Wallet.Add HackingDevice_Access 1000
Rogue.Wallet.Add AssaultRifle_Ammo 1000
Rogue.Wallet.Add LightMachineGun_Ammo 1000
Rogue.Wallet.Add MarksmanRifle_Ammo 1000
Rogue.Wallet.Add Pistol_Ammo 1000
Rogue.Wallet.Add Shotgun_Ammo 1000
Rogue.Wallet.Add SubMachineGun_Ammo 1000
Rogue.Wallet.Add Grenade_EMP_Ammo 1000
Rogue.Wallet.Add Grenade_Flashbang_Ammo 1000
Rogue.Wallet.Add Grenade_HE_Ammo 1000
Rogue.Wallet.Add Grenade_Incendiary_Ammo 1000
Rogue.Wallet.Add Medkit_Ammo 1000
Rogue.Wallet.Add Cloth_Crafting 1000
Rogue.Wallet.Add Cloth_2_Crafting 1000
Rogue.Wallet.Add Cloth_3_Crafting 1000
Rogue.Wallet.Add Cloth_4_Crafting 1000
Rogue.Wallet.Add Cloth_5_Crafting 1000
Rogue.Wallet.Add Weapon_Parts_Crafting 1000
Rogue.Wallet.Add Weapon_Parts_2_Crafting 1000
Rogue.Wallet.Add Weapon_Parts_3_Crafting 1000
Rogue.Wallet.Add Weapon_Parts_4_Crafting 1000
Rogue.Wallet.Add Weapon_Parts_5_Crafting 1000
Rogue.Wallet.Add Tools_Crafting 1000
Rogue.Wallet.Add Tools_2_Crafting 1000
Rogue.Wallet.Add Tools_3_Crafting 1000
Rogue.Wallet.Add Tools_4_Crafting 1000
Rogue.Wallet.Add Tools_5_Crafting 1000
Rogue.Wallet.Add Electronics_Crafting 1000
Rogue.Wallet.Add Electronics_2_Crafting 1000
Rogue.Wallet.Add Electronics_3_Crafting 1000
Rogue.Wallet.Add Electronics_4_Crafting 1000
Rogue.Wallet.Add Electronics_5_Crafting 1000
Rogue.Wallet.Add DarkZone_Crafting 1000
Rogue.Wallet.Add DarkZone_2_Crafting 1000
Rogue.Wallet.Add DarkZone_3_Crafting 1000
Rogue.Wallet.Add DarkZone_4_Crafting 1000
Rogue.Wallet.Add DarkZone_5_Crafting 1000
Rogue.Wallet.Add Improve_Crafting 1000
Rogue.Wallet.Add Bandages_Crafting 1000
Rogue.Wallet.Add Metal_Parts_Crafting 1000
Rogue.Wallet.Add Casing_Crafting 1000
Rogue.Wallet.Add Epic_Crafting 1000
Rogue.Wallet.Add Legendary_Crafting 1000
Rogue.Wallet.Add Supplies 1000
Rogue.Wallet.Add Test_District_1_Security 1000
Rogue.Wallet.Add Security_1 1000
Rogue.Wallet.Add Security_2 1000
Rogue.Wallet.Add Security_3 1000
Rogue.Wallet.Add Security_4 1000
Rogue.Wallet.Add Defibrillator 1000
Rogue.Wallet.Add Grenade_Shock_Ammo 1000
Rogue.Wallet.Add Grenade_TearGas_Ammo 1000
Rogue.Wallet.Add Medical_Points 1000
Rogue.Wallet.Add Tech_Points 1000
Rogue.Wallet.Add Security_Points 1000
Rogue.Wallet.Add Ability_Point 1000
Rogue.Wallet.Add Consumable_Drink_Healing 1000
Rogue.Wallet.Add Consumable_Drink_StatusEffects 1000
Rogue.Wallet.Add Consumable_Food_Damage 1000
Rogue.Wallet.Add Consumable_Food_Cooldowns 1000
Rogue.Wallet.Add Consumable_Ammo_Incendiary 1000
Rogue.Wallet.Add Consumable_Ammo_Explosive 1000
