script.begin name="brand_pb7_john"
rogue.waitForInGame

script.wait time="1"
console.cmd cmd="Rogue.SetLevel 3"

script.wait time="2"

console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_face_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_chest_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_back_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_hands_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_thighs_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_knees_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_hat_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_scarf_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_shirt_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_jacket_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_pants_john Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_shoes_john Green"

console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_shotgun_m4_t3_v1 Orange"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_kobra_scope"

script.wait time="1"

console.cmd cmd="Rogue.EquipItem brand_pb7_face_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_chest_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_back_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_hands_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_thighs_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_knees_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_hat_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_scarf_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_shirt_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_jacket_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_pants_john"
console.cmd cmd="Rogue.EquipItem brand_pb7_shoes_john"

script.end