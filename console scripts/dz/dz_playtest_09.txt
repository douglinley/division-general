script.begin name="dz_playtest_c"

rogue.waitForInGame

script.wait time="1"
console.cmd cmd="Rogue.SetLevel 30"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_back_generated_item Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_chest_generated_item Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_hands_generated_item Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_knees_generated_item Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_thighs_generated_item Purple"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_back_generated_item"
console.cmd cmd="Rogue.EquipItem player_chest_generated_item"
console.cmd cmd="Rogue.EquipItem player_hands_generated_item"
console.cmd cmd="Rogue.EquipItem player_knees_generated_item"
console.cmd cmd="Rogue.EquipItem player_thighs_generated_item"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_submachinegun_821_t1_v1 Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_m1a_t1_v1 Purple"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_v1 Purple"

script.wait time="1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_submachinegun_821_t1_v1 0"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_shotgun_870_t1_v1 1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_pistol_m9_t1_v1 2"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_weapon_submachinegun_821_t1_v1"

script.wait time="1"
console.cmd cmd="Rogue.UnlockFastTravel"

script.wait time="1"
console.cmd cmd="Rogue.SetPosition -72.0,0.13,686.0"

script.wait time="1"
console.cmd cmd="Rogue.SetDarkZoneRank 30"

script.end

This script's spawn point is "CP EAST 8" (Middle East Dark Zone Checkpoint in Samuel, "Rogue.SetPosition 198.8,1.0,430.1"). It is now set to be right in the middle of the Heart section of the Dark Zone for compliance testing.