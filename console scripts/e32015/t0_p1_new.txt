script.begin name="t0_p1_new.txt"
console.cmd cmd="Rogue.E32015.ConsoleScriptStart"
rogue.waitForInGame
console.cmd cmd="Rogue.DiscoverAllPSO"
console.cmd cmd="Rogue.Inventory.ClearStash"

console.cmd cmd="Rogue.SetLevel 12"
console.cmd cmd="Rogue.XP.Add 32730"

console.cmd cmd="Rogue.E32015.SetTeam 0"
console.cmd cmd="Rogue.E32015.SetMember 1"
console.cmd cmd="Rogue.E32015.SetLeader 1"

console.cmd cmd="Rogue.CustomizeProfile isMale = true, values = { 2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.75, 0.5, 0.967213, 0, 0.5, 0.566926, 0, 0.5, 0.0867516, 0, 1, 0, 0, 0.885505, 0, 0, 0, 0, 0, }"
script.wait time="1"
console.cmd cmd="Rogue.CustomizeProfile isMale = true, values = { 2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.75, 0.5, 0.967213, 0, 0.5, 0.566926, 0, 0.5, 0.0867516, 0, 1, 0, 0, 0.885505, 0, 0, 0, 0, 0, }"

console.cmd cmd="Rogue.DamageSelf -20000"

console.cmd cmd="Rogue.Item.SpawnInInventory access_resource_item_dark_zone_key"

console.cmd cmd="Rogue.Item.SpawnAndEquip brand_face_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_chest_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_back_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_hands_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_thighs_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_knees_player_bronson Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_hat_player_bronson Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_scarf_player_bronson Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_shirt_player_bronson Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_jacket_player_bronson Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_pants_player_bronson Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_shoes_player_bronson Green"

console.cmd cmd="Rogue.Item.SpawnAndEquip Player_weapon_pistol_px4_t1_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip Player_weapon_lightmachinegun_m249_t3_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip Player_weapon_shotgun_m4_t1_v1 Blue"

console.cmd cmd="Rogue.Talent.AssignSkillToLB player_pulse"
console.cmd cmd="Rogue.Talent.AssignSkillToRB player_turret"

console.cmd cmd="Rogue.E32015.ConsoleScriptEnd"
script.end