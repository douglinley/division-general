script.begin name="dz_ld_playtest_2"

rogue.waitForInGame

script.wait time="1"
console.cmd cmd="Rogue.SetLevel 30"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_back_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_chest_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_hands_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_knees_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_thighs_generated_item Green"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_back_generated_item"
console.cmd cmd="Rogue.EquipItem player_chest_generated_item"
console.cmd cmd="Rogue.EquipItem player_hands_generated_item"
console.cmd cmd="Rogue.EquipItem player_knees_generated_item"
console.cmd cmd="Rogue.EquipItem player_thighs_generated_item"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_assaultrifle_ACR_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_m1a_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_acogscope Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_cqbss_scope Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_vx1scope Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_mk5scope Blue"

script.wait time="1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_assaultrifle_ACR_t1_v1 0"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_marksmanrifle_m1a_t1_v1 1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_pistol_m9_t1_v1 2"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_weapon_assaultrifle_ACR_t1_v1"

script.wait time="1"
console.cmd cmd="Rogue.Skill.UnlockAll"

script.wait time="1"
console.cmd cmd="Rogue.SetPosition -551.3,1.9,1318.1"

//spawn contaminated item for DZ extraction test.
script.wait time="5"
console.cmd cmd="Rogue.Item.SpawnInInventory player_chest_generated_item Purple"

script.end

This script's spawn point is custom for LD playtesting specific areas and are subject to change between playtests.
