// %1 language id, %2 command, %3+ arguments

console.cmd cmd="core.localization.mainlanguage %1"

script.wait time=3

script.exec blocking=true filename="%2+"
