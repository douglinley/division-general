include "rogue/game system data/fruit/ai/ai.fruit"
include "rogue/game system data/fruit/base of operation/baseofoperation.fruit"
include "rogue/game system data/fruit/faction/faction.fruit"
include "rogue/game system data/fruit/interaction/interactionparams.fruit"
include "rogue/game system data/fruit/ui/inventory_categories.fruit"
include "rogue/game system data/fruit/item/items.fruit"
include "rogue/game system data/fruit/loot/lootquality.fruit"
include "rogue/game system data/fruit/mission/mission.fruit"
include "rogue/game system data/fruit/ai/npctypes.fruit"
include "rogue/game system data/fruit/statuseffects/statuseffects.fruit"
include "rogue/skillscript/skillscript.fruit"
include "rogue/skillscript/skillscriptarchetypes.fruit"
include "rogue/game system data/fruit/class/xpsystem.fruit"

class CiceroCondition < abstract=1 >
{
}

list CiceroConditionList
{
	CiceroCondition
}

enum CiceroMissionProgressionType
{
	Mission_Succeded
	Mission_Failed
	Mission_InProgress
	Mission_IdleLocked
	Mission_IdleUndiscovered
	Mission_IdleAvailable
}

enum CiceroBooRewardsType
{
	Reward_Skill
	Reward_SkillMod
	Reward_Talent
}

list CurrencyCiceroConditionList
{
	CurrencyDefinition* 
}

class ActiveWeaponIsSlotCondition < tooltip="Passes when the active weapon matches with the weapon slot if the myIsActive value is true." > : CiceroCondition
{
	InventoryWeaponSetSlots myWeaponSlot PrimaryWeaponSlot < tooltip="The weapon slot to test." >
	bool myIsActive TRUE < tooltip="TRUE to pass when slot is empty, FALSE to pass when slot is occupied." >
}

class AffectedByStatusEffectCondition < tooltip="Passes when the player's affected by status effect state matches the value set." > : CiceroCondition
{
	StatusEffect* myStatusEffect < tooltip="The status effect to test against." >
	bool myAffectedByStatusEffect TRUE  < tooltip="TRUE to pass when affected by this status effect, FALSE to pass when unaffected by it." >
}

class AnyCondition < tooltip="Passes when ANY of the sub-conditions pass (use this like a multi-OR node)." > : CiceroCondition
{
	CiceroConditionList mySubConditions
}

class AttributeWithinRangeCondition < tooltip="Passes when the specified attribute value falls within the specified range (INCLUSIVE)." > : CiceroCondition
{
	uid myAttribute < bhv=attributeList >
	float myLowerBound_(Inclusive) < tooltip="Lower end of range for attribute value to fall within (INCLUSIVE)." >
	float myUpperBound_(Inclusive) < tooltip="Upper end of range for attribute value to fall within (INCLUSIVE)." >
}
class BooRewardIsAvailableToPurchaseCondition < tooltip="Passes when there is a reward type of the specified Boo Reward Type." > : CiceroCondition
{
	CiceroBooRewardsType myBooRewardType < tooltip="Type of reward we are interested on teaching about." >
}

class BooUpgradeIsBuiltCondition < tooltip="Passes when the specified Wing Upgrade has been unlocked." > : CiceroCondition
{
	WingUpgrade* myWingUpgrade < tooltip="The wing upgraded we are interested in knowing if it is unlocked." >
	bool myIsBuilt TRUE < tooltip="Passes if the unlocking status matches the value of this variable." >
}

class CanShareAnItemWithGroupCondition < tooltip="Passes when an item can be shared with at least one other group member, and if the player has not shared an item for X amount of time." > : CiceroCondition
{
	float myTime 30.0 < tooltip="Time in seconds passed without the player sharing an item required for this condition to pass." >
}

class ClosestFriendlyIsInRangeCondition < tooltip="Passes when thre is at least one agent in the party inside the range specified." > : CiceroCondition
{
	float myRange < min=0.0 tooltip="The minimum value of the party member distance." >
	bool myIsTrue TRUE < tooltip="If true passes when the condition is true. If false passes when the condition is false" >		
}


class ClosestSpawnPointsIsInRangeCondition < tooltip="Passes when thre is at least one spawn point inside outside the range specified." > : CiceroCondition
{
	float myLowerBound < min=0.0 tooltip="The minimum value of the spawn point distance." >
	float myUpperBound < min=0.0 tooltip="The maximum value of the spawn point distance." >
	bool myIsTrue TRUE < tooltip="If true passes when the condition is true. If false passes when the condition is false" >		
}

class CurrencyAmountCondition < tooltip="Passes if the agent's currency of specified type exceeds the value specified." > : CiceroCondition
{
	CurrencyCiceroConditionList  myCurrencyDefinitions < tooltip="The currencies to test against." >
	bool myIsAccumulative TRUE < tooltip="If true the value of each currency will be added... If False the lowerbound will be compared against the maximum value." >	 
	int myLowerBound < tooltip="The amount of currency that must be exceeded for the condition to pass." >
}

class TookDamageOutOfCoverCondition < tooltip="Passes when the agent has taken a certain percentage of their max health in damage in the specified time window." > : CiceroCondition
{
	float myDamageOutOfCoverPercentage 0.1 < min=0.01 max=1.00 step=0.01 tooltip="The percentage of the agent's max health that has been lost." >
	float myTimespan 5.0 < min=1.0 max=180.0 tooltip="Only look at the damage delivered in this timespan (seconds)." >
}

class DoesPlayerHaveAnyActiveMissionCondition < tooltip="Passes if the player is in any mission." > : CiceroCondition
{
	bool myIsTrue TRUE < tooltip="True if the player has started any mission." >
}

class DZAlignmentCondition < tooltip="Passes if the agent's DarkZone Rogue status matches the value set." > : CiceroCondition
{
	bool myIsRogue TRUE < tooltip="TRUE to pass when rogue, FALSE to pass when not rogue." >
}

class DZPartyMemberInDifferentPhaseCondition < tooltip="Passes if the agent has any party member who is in a different DZ phase to them." > : CiceroCondition
{
	bool myIsInDifferentPhase TRUE < tooltip="TRUE to pass when ANY member is in a different phase, FALSE to pass when NO member is in a different phase." >
}

class DZPlayerDroppedItemsFullCondition < tooltip="Passes if the agent has dropped more than the specified number of items in DarkZone." > : CiceroCondition
{
	int myDroppedItemsLowerBound < tooltip="The number of items to be dropped for the condition to pass." >
}

class DZPlayerLostLevelsCondition < tooltip="Passes if the agent has lost more than the specific number of levels due to death in the DarkZone." > : CiceroCondition
{
	int myLostLevelsLowerBound	< min=1 tooltip="The number of levels to be lost before the condition can pass." >
}

class DZRogueStageCondition < tooltip="Passes if the agent's Rogue Stage is higher than the value specified." > : CiceroCondition
{
	int	myRogueStageLowerBound < tooltip="The level of Rogue Stage that must be exceeded for the condition to pass." >
}

class EnterOverviewAtLevel30Condition < tooltip="Passes when entering the overview when over lvl 30" > : CiceroCondition
{
}

class EquipItemWithGearGradeCondition < tooltip="Passes when equipping an item with a gear grade greater than 0 and entering the correct inventory category" > : CiceroCondition
{
}

class EquippedGearHasUnusedModSlotsCondition < tooltip="Passes if the agent has at least one empty mod slot on their equipped gear (in any slot!)." > : CiceroCondition
{
}

class EquippedWeaponLessLevelsThanCondition < tooltip="Passes when the player has an equipped weapon whose level requirement is less than X levels than Player Level." > : CiceroCondition
{
	int myLevelsDifference	< min=0 tooltip="The minimum number of levels less than player that equipped weapon has to have." >
}

class EquippedGearLessLevelsThanCondition < tooltip="Passes when the player has an equipped gear whose level requirement is less than X levels than Player Level." > : CiceroCondition
{
	int myLevelsDifference	< min=0 tooltip="The minimum number of levels less than player that equipped gear has to have." >
}

class FacingDamagingFallCondition < tooltip="Passes when the player has a prompt for a drop that would cause fall damage." > : CiceroCondition
{
}

class FriendlyIsDownedNearbyCondition < tooltip="Passes when a friendly or neutral is downed nearby." > : CiceroCondition
{
	float myRange 5.0 < min=0.0 max=500.0 tooltip="The distance (in metres) classed as nearby." >
}

class HasAggressorOfRoleCondition < tooltip="Passes when the player is in combat with at least one NPC of the specified role." > : CiceroCondition
{
	NPCSpecialRole myRole < tooltip="The role of the NPC aggressor to test against." >
}

class HasAScopeOnEquippedWeaponCondition < tooltip="Passes when the player has a scope attacted to the equipped weapon." > : CiceroCondition
{
	bool myHasAScopeOnEquippedWeapon TRUE < tooltip="TRUE to pass when a scope is on the equipped weapon, FALSE to pass when a scope isn't on the equipped weapon." >
}

class HasAvailableGearModsCondition < tooltip="Passes when the player has gear mods in their inventory that have not been attached to gear." > : CiceroCondition
{
}

class HasBeenDetectedByEnemyCondition < tooltip="Passes when the player has been detected by an enemy." > : CiceroCondition
{
	bool myHasBeenDetected TRUE < tooltip="TRUE to pass when detected, FALSE to pass when not detected." >
}

class HasBestInteractionOfTypeCondition < tooltip="Passes when the player's 'best' interaction (closest & most eligible, visible on UI) matches the specified type." > : CiceroCondition
{
	InteractionParams* myInteractionType < tooltip="The type of interaction that must be active for this condition to pass." >
}

class HasEverEquippedSkillCondition < tooltip="Passes when a skill has been equipped in the past." > : CiceroCondition
{
	SkillScript* mySkillScript < tooltip="The SkillScript to test against." >
	bool myHasBeenEquipped TRUE < tooltip="TRUE to pass when a skill has been equipped in the past, FALSE to pass when a skill has never been equipped." >
}

class HasItemOfQualityCondition < tooltip="Passes when the agent has at least 1 item of any type (including weapons and mods) in their inventory." > : CiceroCondition
{
	ItemQuality myItemQuality < tooltip="The quality to test against." >
}

class HasHigherLevelAggressorCondition < tooltip="Passes when player is on the threat list of an NPC agent who is 'level difference' levels (or greater) higher than them." > : CiceroCondition
{
	int myLevelDifference 1 < min=0 max=100 tooltip="The number of levels higher than you the NPC must be for this condition to pass." >
}

class HasNewCollectablesAvailable < tooltip="Passes when player has new collectables to see in the menu." > : CiceroCondition
{
}

class HasNewMissionNarrativesAvailable < tooltip="Passes when player has new mission narratives to see on the menu." > : CiceroCondition
{
}

class HasNewItemInInventoryPouch < tooltip="Passes when player has new items in any of the inventory pouches specified." > : CiceroCondition
{
	InventoryPouchTypes myPouch DefaultItemPouch < tooltip="The inventory pouches that this tip will get evaluated against." >
}

class HasNewWeaponModsAvailable < tooltip="Passes when player has any new weapon mod item on the inventory." > : CiceroCondition
{
}
				
class InCombatCondition < tooltip="Passes when the agent's in combat state matches the value set." > : CiceroCondition
{
	bool myInCombat TRUE < tooltip="TRUE to pass when in combat, FALSE to pass when out of combat." >
}

class InventoryFullCondition < tooltip="Passes if the agent's specified inventory pouch type's fullness matches the value set." > : CiceroCondition
{
	InventoryPouchTypes myInventoryPouchType DefaultItemPouch < tooltip="The inventory pouch type to test for fullness." >
	bool myIsFull TRUE < tooltip="TRUE to pass if selected inventory pouch is full, FALSE to pass if selected inventory pouch is not full." >
}

class IsAliveCondition < tooltip="Passes when agent 'alive' status matches the value set here." > : CiceroCondition
{
	bool myIsAlive TRUE < tooltip="TRUE to pass when agent is alive, FALSE to pass when agent is dead." >
}

class IsDownedCondition < tooltip="Passes when the player is downed." > : CiceroCondition
{
	bool myIsDowned TRUE < tooltip="TRUE to pass when downed, FALSE to pass when not downed." >
}

class IsInBaseOfOperationsCondition < tooltip="Passes when the player is in the base of operations." > : CiceroCondition
{
	bool myIsInBaseOfOperations TRUE < tooltip="TRUE to pass when in the base of operations, FALSE to pass when out of the base of operations." >
}

class IsInMissionVolumeCondition < tooltip="Passes when the player is in any mission volume." > : CiceroCondition
{
	bool myIsInMissionVolume TRUE < tooltip="TRUE to pass when in any mission volume, FALSE to pass when out of any mission volume." >
}

class IsInPartyCondition < tooltip="Passes when the player being on a party matches the value of myIsInParty." > : CiceroCondition
{
	bool myIsInParty TRUE < tooltip="TRUE to pass when player is part of a party." >
}

class KilledByCondition < tooltip="Passes if agent was killed by an NPC of the specified archetype." > : CiceroCondition
{
	NPCArchetype	   myNPCArchetype < tooltip="The archetype of the NPC you wish the test to pass against." >
}

class KilledByPlayerCondition < tooltip="Passes if the agent was killed by a player, or an NPC, depending on the value set." > : CiceroCondition
{
	bool myIsTrue TRUE < tooltip="TRUE to pass when killed by a player, FALSE to pass when killed by an NPC" >
}

class LastKilledByFactionCondition < tooltip="Passes if the agent was last killed by an NPC of the specified faction." > : CiceroCondition
{
	Faction* myFaction < tooltip="The faction to test against." >
}

class LowOnAmmoCondition < tooltip="Passes if the agent's eqiupped weapon's current ammo percentage is below the specified value." > : CiceroCondition
{
	float myLowAmmoThresholdPercentage 0.15 < min=0.0 max=1.0 step=0.01 tooltip="Below this percentage the condition will pass." >
}

class ManualTriggerCondition < tooltip="Passes when the condition is 'triggered' by another code or data source." > : CiceroCondition
{
}

class MissionHasEverBeenCompletedCondition < tooltip="Passes if the mission has ever been completed by player." > : CiceroCondition
{
	Mission* myMissionId < tooltip="Specifies the mission that must be for the condition to pass." >
	bool myIsTrue TRUE < tooltip="If true passes when the condition is true. If false passes when the condition is false" >	
}

class Mission_ActivityCondition < tooltip="Passes if the agent activity with the specified mission matches." > : CiceroCondition
{
	Mission* myMissionId < tooltip="Specifies the mission that must be active for the condition to pass." >
	CiceroMissionProgressionType myMissionActivityType Mission_InProgress	< tooltip="Specifies the Activity type we are interested in evaluating." >	 
}

class Mission_ActivityPerTypeCondition < tooltip="Passes if the any of the active missions belong to the specified types." > : CiceroCondition
{
	MissionTypesList myMissionTypes < tooltip="Specifies the mission types." >
	CiceroMissionProgressionType myMissionActivityType Mission_Succeded	< tooltip="Specifies the Activity type we are interested in evaluating." >
	bool myIsTrue TRUE < tooltip="If true passes when the condition is true. If false passes when the condition is false" >
}

class PlayerHealthIsInRangeCondition < tooltip="Passes if the agent's health is inside the range specified in the condition." > : CiceroCondition
{
	float myLowerBound < min=0.0 max=1.0 tooltip="The minimum value of the health in percetange." >
	float myUpperBound < min=0.0 max=1.0 tooltip="The maximum value of the health in percetange." >
}

class PlayerAirSupplyIsInRangeCondition < tooltip="Passes if the agent's air supply is inside the range specified in the condition." > : CiceroCondition
{
	float myLowerBound < min=0.0 max=1.0 tooltip="The minimum value of the air supply in percetange." >
	float myUpperBound < min=0.0 max=1.0 tooltip="The maximum value of the air supply in percetange." >
}

class PlayerLevelIsInRangeCondition < tooltip="Passes if the agent player level is inside the range specified in the condition." > : CiceroCondition
{
	int myLowerBound < min=0 tooltip="The minimum value of the player level." >
	int myUpperBound < min=0 tooltip="The maximum value of the player level." >
	bool myIsTrue TRUE < tooltip="If true passes when the condition is true. If false passes when the condition is false" >	
	XPTypes myXPType NormalXP < tooltip="The XP Type you want to evaluate versus" >
}
class PlayerOnExtractionCondition < tooltip="Passes if the agent's 'in extraction area' flag matches the value set." > : CiceroCondition
{
	bool myIsOnExtractionPoint TRUE < tooltip="TRUE to pass when in extraction area, FALSE to pass when not in extraction area." >
}

class SecondaryWeaponSlotUnlockedCondition < tooltip="Passes when the players secondary weapon slot is unlocked." > : CiceroCondition
{
	bool myIsUnlocked TRUE < tooltip="TRUE to pass when secondary slot is unlocked, FALSE to pass when secondary slot is locked." >
}

class SignatureSkillsUnlockedCondition < tooltip="Passes when the sig-skill unlock status matches the value set." > : CiceroCondition
{
	bool myIsUnlocked TRUE < tooltip="TRUE to pass when signature skills are unlocked, FALSE to pass when they are locked." >
}

class SignatureSkillOnCooldownCondition < tooltip="Passes when the specified sig-skill archetype 'on cooldown' status matches the value set." > : CiceroCondition
{
	SkillScriptArchetype mySignatureSkillArchetype < tooltip="The sig-skill archetype to test for cooldowns ('Tactics' and 'None' will never work here)." >
	bool myIsOnCooldown TRUE < tooltip="TRUE to pass when sig-skill is on cooldown, FALSE to pass when sig-skill not on cooldown." >
}

class SkillAssignedCondition < tooltip="Passes if the assigned state of the specified skillscript matches the value set." > : CiceroCondition
{
	SkillScript* mySkillScript < tooltip="The SkillScript to test against." >
	bool myIsAssigned TRUE < tooltip="TRUE to pass if the specified skillscript is assigned, FALSE to pass if the specified skillscript is not assigned." >
}

class SkillAssignedToSlotCondition < tooltip="Passes if the skillscript is assigned to the given skill slot." > : CiceroCondition
{
	SkillScript* mySkillScript < tooltip="The SkillScript to test against." >
	int mySkillScriptSlotIndex 0 < min=0 max=1 tooltip="0 = right slot, 1 = left slot" >	
}

class SkillSlotOnCooldownCondition < tooltip="Passes when the specified skill slot 'on cooldown' status matches the value set." > : CiceroCondition
{
	int mySkillScriptSlotIndex 0 < min=0 max=2 tooltip="0 = right slot, 1 = left slot, 2 = signature skill slot" >
	bool myIsOnCooldown TRUE < tooltip="TRUE to pass when skill slot is on cooldown, FALSE to pass when skill slot not on cooldown." >
}

class SkillSlotIsEmptyCondition < tooltip="Passes when the agent's specified skillscript slot status (empty or not) matches the value set." > : CiceroCondition
{
	int mySkillScriptSlotIndex 0 < min=0 max=2 tooltip="0 = right slot, 1 = left slot, 2 = signature skill slot" >
	bool myIsEmpty TRUE < tooltip="TRUE to pass when slot is empty, FALSE to pass when slot is occupied." >
}

class SkillUnlockedCondition < tooltip="Passes if the agent has unlocked the specified skillscript." > : CiceroCondition
{
	SkillScript* mySkillScript < tooltip="The SkillScript to test against." >
}

class UsingInteractionOfTypeCondition < tooltip="Passes when the player is actively interacting with an interaction of the specified type." > : CiceroCondition
{
	InteractionParams* myInteractionType < tooltip="The type of interaction that must be active for this condition to pass." >
}


class WeaponHasUnlockablesCondition < tooltip="Passes when the equipped weapon has unlockables associated with it." > : CiceroCondition
{
}

class WeaponHasInactiveUnlockablesCondition < tooltip="Passes when the equipped weapon has at least one unlockable that isn't active." > : CiceroCondition
{
}

class WeaponSlotEmptyCondition < tooltip="Passes when the specified weapon slots empty status matches the value set." > : CiceroCondition
{
	InventoryWeaponSetSlots myWeaponSlot PrimaryWeaponSlot < tooltip="The weapon slot to test." >
	bool mySlotIsEmpty TRUE < tooltip="TRUE to pass when slot is empty, FALSE to pass when slot is occupied." >
}