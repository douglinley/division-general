include "rogue/game system data/fruit/ai/noncombatantdetectionprofiles.fruit"

NonCombatantDetectionProfile DefaultNonCombatantDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 5000
		myDecayRate 20.0
	}
	myConcernedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 6000
		myDecayRate 20.0
	}
	myScaredValues
	{
		myThreshold 120.0
		myPauseBeforeValueDecay 7000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 12000
		myDecayRate 20.0
	}
}

NonCombatantDetectionProfile CivilianBasicDetectionProfile
{
	myAwareValues
	{
		myThreshold 10.0
		myPauseBeforeValueDecay 2000
		myDecayRate 0.0
		myIncreaseDelay 0
	}
	myConcernedValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 60000
		myDecayRate 0.0
		myIncreaseDelay 3000
		myInheritThreatRange 5.0
		myInheritThreatDuration 10000
	}
	myScaredValues
	{
		myThreshold 300.0
		myPauseBeforeValueDecay 60000
		myDecayRate 0.0
		myIncreaseDelay 5000
		myInheritThreatRange 10.0
		myInheritThreatDuration 10000
	}
	myPanickedValues
	{
		myThreshold 400.0
		myPauseBeforeValueDecay 60000
		myDecayRate 0.0
		myIncreaseDelay 5000
		myInheritThreatRange 10.0
		myInheritThreatDuration 10000
	}
	mySoundAlertLevels
	{
		SoundAlertLevels "Threatening Sounds"
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				AlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 20.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Alert Range"
				{
					myMaxDistance 1.0
					myAlertLevel Concerned
					myMinSpeed 0.75
				}
				VisionAlertLevelDetectionRange "Aware Alert Range"
				{
					myMaxDistance 50.0
					myAlertLevel Aware
				}
			}
		}
		VisionAlertLevels Suspicious
		{
			myFactionRelation Suspicous
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 20.0
					myAlertLevel Concerned
				}
				VisionAlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 10.0
					myAlertLevel Scared
				}
				VisionAlertLevelDetectionRange "Panicked Range"
				{
					myMaxDistance 5.0
					myAlertLevel Panicked
				}
			}
		}
	}
	myAimedAtAlertLevels
	{
		AimedAtAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Concerned
				{
					myMaxDistance 30.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 15.0
					myAlertLevel Scared
				}
			}
		}
		AimedAtAlertLevels Suspicious
		{
			myFactionRelation Suspicous
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Concerned
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 10.0
					myAlertLevel Scared
				}
			}
		}
	}
	myAimedAtMaxRadius 3.0
	myShotAtAlertLevel Panicked
	myExitSequenceMaxAlertLevel Scared
	myDisableVisionDuringInteraction TRUE
	myDisableVisionAfterInteractionDuration 20000
	myAlertZoneVisionRequiresLOS FALSE
	myIsBully FALSE
	myInheritThreat TRUE
	myLowestInheritedAlertLevel Concerned
	myInheritThreatDelay 1000
	myInheritThreatArchetypes
	{
		NPCArchetype "Civilian Standard" Civilian
		NPCArchetype "Civilian Sick" CivilianSick
	}
}

NonCombatantDetectionProfile AUTOTEST_CivilianDetectionProfile
{
	myAwareValues
	{
		myThreshold 100.0
	}
	myConcernedValues
	{
		myThreshold 100.0
	}
	myScaredValues
	{
		myThreshold 100.0
	}
	myPanickedValues
	{
		myThreshold 100.0
	}
	mySoundAlertLevels
	{
		SoundAlertLevels "Threatening Sounds"
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				AlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 20.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Alert Range"
				{
					myMaxDistance 2.0
					myAlertLevel Concerned
					myMinSpeed 1.5
				}
				VisionAlertLevelDetectionRange "Aware Alert Range"
				{
					myMaxDistance 50.0
					myAlertLevel Aware
				}
			}
		}
		VisionAlertLevels Suspicious
		{
			myFactionRelation Suspicous
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 20.0
					myAlertLevel Concerned
				}
				VisionAlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 10.0
					myAlertLevel Scared
				}
				VisionAlertLevelDetectionRange "Panicked Range"
				{
					myMaxDistance 5.0
					myAlertLevel Panicked
				}
			}
		}
	}
	myAimedAtAlertLevels
	{
		AimedAtAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Concerned
				{
					myMaxDistance 30.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 15.0
					myAlertLevel Scared
				}
			}
		}
		AimedAtAlertLevels Suspicious
		{
			myFactionRelation Suspicous
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Concerned
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 10.0
					myAlertLevel Scared
				}
			}
		}
	}
	myAimedAtMaxRadius 3.0
	myShotAtAlertLevel Panicked
	myExitSequenceMaxAlertLevel Scared
	myDisableVisionDuringInteraction TRUE
	myDisableVisionAfterInteractionDuration 3000
	myIsBully FALSE
	myInheritThreat TRUE
	myLowestInheritedAlertLevel Concerned
	myInheritThreatDelay 1000
	myInheritThreatArchetypes
	{
		NPCArchetype "Civilian Standard" Civilian
		NPCArchetype "Civilian Sick" CivilianSick
	}
}

NonCombatantDetectionProfile CivilianFearfulDetectionProfile
{
	myAwareValues
	{
		myThreshold 10.0
		myPauseBeforeValueDecay 2000
		myDecayRate 50.0
	}
	myConcernedValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 60000
		myDecayRate 0.0
		myIncreaseDelay 3
	}
	myScaredValues
	{
		myThreshold 300.0
		myPauseBeforeValueDecay 60000
		myDecayRate 5.0
	}
	myPanickedValues
	{
		myThreshold 400.0
		myPauseBeforeValueDecay 60000
		myDecayRate 0.0
		myIncreaseDelay 5
	}
	mySoundAlertLevels
	{
		SoundAlertLevels "Threatening Sounds"
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				AlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
				AlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 20.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Alert Range"
				{
					myMaxDistance 5.0
					myAlertLevel Concerned
				}
			}
		}
		VisionAlertLevels Suspicious
		{
			myFactionRelation Suspicous
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange "Concerned Range"
				{
					myMaxDistance 20.0
					myAlertLevel Concerned
				}
				VisionAlertLevelDetectionRange "Scared Range"
				{
					myMaxDistance 10.0
					myAlertLevel Scared
				}
				VisionAlertLevelDetectionRange "Panicked Range"
				{
					myMaxDistance 5.0
					myAlertLevel Panicked
				}
			}
		}
	}
	myShotAtAlertLevel Scared
	myIsBully FALSE
	myInheritThreat TRUE
	myLowestInheritedAlertLevel Concerned
	myInheritThreatDelay 1000
}

NonCombatantDetectionProfile DogDetectionProfile
{
	myAwareValues
	{
		myThreshold 22.0
		myPauseBeforeValueDecay 5000
		myDecayRate 8.0
	}
	myConcernedValues
	{
		myThreshold 35.0
		myPauseBeforeValueDecay 6000
		myDecayRate 5.0
	}
	myScaredValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 18000
		myDecayRate 0.0
		myInheritThreatRange 20.0
		myInheritThreatDuration 50000
	}
	myPanickedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 15000
		myDecayRate 0.0
		myInheritThreatRange 20.0
		myInheritThreatDuration 50000
	}
	mySoundAlertLevels
	{
		SoundAlertLevels Threatening
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 50.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels "Dog VS Friendly"
		{
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 8.0
					myAlertLevel Aware
				}
				VisionAlertLevelDetectionRange Concerned
				{
					myMaxDistance 3.0
					myAlertLevel Concerned
				}
			}
		}
		VisionAlertLevels "Dog VS Neutral"
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Concerned
				{
					myMaxDistance 9.0
					myAlertLevel Concerned
				}
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 16.0
					myAlertLevel Aware
				}
			}
		}
	}
	myShotAtAlertLevel Panicked
	myBullyCooldown 1
	myInheritThreat TRUE
	myLowestInheritedAlertLevel Scared
	myInheritThreatDelay 500
}

NonCombatantDetectionProfile RavenDetectionProfile
{
	myAwareValues
	{
		myThreshold 1.0
		myPauseBeforeValueDecay 1000
		myDecayRate 5.0
	}
	myConcernedValues
	{
		myThreshold 2.0
		myPauseBeforeValueDecay 1000
		myDecayRate 5.0
	}
	myScaredValues
	{
		myThreshold 3.0
		myPauseBeforeValueDecay 1000
		myDecayRate 5.0
	}
	myPanickedValues
	{
		myThreshold 4.0
		myPauseBeforeValueDecay 1000
		myDecayRate 5.0
	}
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
}

NonCombatantDetectionProfile RatDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 3000
		myDecayRate 500.0
	}
	myConcernedValues
	{
		myThreshold 4000.0
		myPauseBeforeValueDecay 3000
		myDecayRate 500.0
	}
	myScaredValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 400.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	mySoundAlertLevels
	{
		SoundAlertLevels Threatening
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Range
				{
					myMaxDistance 30.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 8.0
					myAlertLevel Concerned
					myMinSpeed 1.0
				}
			}
		}
		VisionAlertLevels Cautious
		{
			myFactionRelation Cautious
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 3.0
					myAlertLevel Aware
				}
			}
		}
	}
	myShotAtAlertLevel Scared
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
	myMaxBullyTime 100
	myBullyCooldown 100
	myRecoveryTime 100
	myIsBully FALSE
	myInheritThreat TRUE
}

NonCombatantDetectionProfile ILSDetectionProfile
{
}

NonCombatantDetectionProfile CivilianBooDetectionProfile
{
	myAwareValues
	{
		myThreshold 10.0
		myPauseBeforeValueDecay 2000
		myDecayRate 20.0
	}
	myConcernedValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 2000
		myDecayRate 2000.0
	}
	myScaredValues
	{
		myThreshold 50000.0
		myPauseBeforeValueDecay 2000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 400.0
		myPauseBeforeValueDecay 2000
		myDecayRate 20.0
	}
	myIsBully FALSE
}

NonCombatantDetectionProfile PigeonDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myConcernedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 1000
		myDecayRate 40.0
	}
	myScaredValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 200.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
}

NonCombatantDetectionProfile SeagullDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myConcernedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 1000
		myDecayRate 40.0
	}
	myScaredValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 200.0
		myPauseBeforeValueDecay 1000
		myDecayRate 20.0
	}
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
}

NonCombatantDetectionProfile Raven01DetectionProfile
{
	myAwareValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 5000
		myDecayRate 500.0
	}
	myConcernedValues
	{
		myThreshold 1000.0
		myPauseBeforeValueDecay 30000
		myDecayRate 100.0
	}
	myScaredValues
	{
		myThreshold 1200.0
		myPauseBeforeValueDecay 10000
		myDecayRate 0.0
	}
	myPanickedValues
	{
		myThreshold 1500.0
		myPauseBeforeValueDecay 10000
		myDecayRate 0.0
	}
	mySoundAlertLevels
	{
		SoundAlertLevels ThreateningLong
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Range
				{
					myMaxDistance 50.0
					myAlertLevel Concerned
				}
			}
		}
		SoundAlertLevels ThreateningShort
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange RangeShort
				{
					myMaxDistance 20.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange AwareRange
				{
					myMaxDistance 20.0
					myAlertLevel Aware
				}
			}
		}
	}
	myShotAtAlertLevel Scared
	myExitSequenceMaxAlertLevel Concerned
	myDisableVisionDuringInteraction FALSE
	myDisableVisionDuringInteractionFactionRelation Neutral
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
	myMaxBullyTime 100
	myBullyCooldown 100
	myRecoveryTime 100
}

NonCombatantDetectionProfile RavenFxDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 5000
		myDecayRate 0.0
	}
	myConcernedValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 5000
		myDecayRate 0.0
	}
	myScaredValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 1000
		myDecayRate 0.0
	}
	myPanickedValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 1000
	}
	mySoundAlertLevels
	{
		SoundAlertLevels Threatening
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange RangeScared
				{
					myMaxDistance 70.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Neutral
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 50.0
					myAlertLevel Aware
				}
			}
		}
	}
	myShotAtAlertLevel Scared
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
	myLowestInheritedAlertLevel Panicked
}

NonCombatantDetectionProfile RatTestDetectionProfile
{
	myAwareValues
	{
		myThreshold 50.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	myConcernedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	myScaredValues
	{
		myThreshold 150.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	myPanickedValues
	{
		myThreshold 400.0
		myPauseBeforeValueDecay 8000
		myDecayRate 20.0
	}
	myVisionRequiresLOS FALSE
	myAlertZoneVisionRequiresLOS FALSE
	myInheritThreat TRUE
}

NonCombatantDetectionProfile CivilianPatrolDetectionProfile
{
	myAwareValues
	{
		myThreshold 10.0
		myPauseBeforeValueDecay 2000
		myDecayRate 200.0
	}
	myConcernedValues
	{
		myThreshold 200000.0
		myPauseBeforeValueDecay 1
		myDecayRate 0.0
	}
	myScaredValues
	{
		myThreshold 250000.0
		myPauseBeforeValueDecay 2000
		myDecayRate 50.0
	}
	myPanickedValues
	{
		myThreshold 300000.0
		myPauseBeforeValueDecay 2000
		myDecayRate 50.0
	}
	myVisionAlertLevels
	{
		VisionAlertLevels Cautious
		{
			myFactionRelation Cautious
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 5.0
					myAlertLevel Aware
				}
			}
		}
	}
	myIsBully FALSE
	myInheritThreat FALSE
}

NonCombatantDetectionProfile DogOutdoorDetectionProfile
{
	myAwareValues
	{
		myThreshold 22.0
		myPauseBeforeValueDecay 5000
		myDecayRate 8.0
	}
	myConcernedValues
	{
		myThreshold 35.0
		myPauseBeforeValueDecay 6000
		myDecayRate 5.0
	}
	myScaredValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 18000
		myDecayRate 0.0
		myInheritThreatRange 15.0
		myInheritThreatDuration 5000
	}
	myPanickedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 15000
		myDecayRate 0.0
		myInheritThreatRange 15.0
		myInheritThreatDuration 5000
	}
	mySoundAlertLevels
	{
		SoundAlertLevels Threatening
		{
			mySoundType Threatening
			myAlertLevelRanges
			{
				AlertLevelDetectionRange Scared
				{
					myMaxDistance 50.0
					myAlertLevel Scared
				}
			}
		}
	}
	myVisionAlertLevels
	{
		VisionAlertLevels "Dog VS Friendly"
		{
			myFactionRelation Friendly
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 8.0
					myAlertLevel Aware
				}
				VisionAlertLevelDetectionRange Concerned
				{
					myMaxDistance 3.0
					myAlertLevel Concerned
				}
			}
		}
		VisionAlertLevels "Dog VS Neutral"
		{
			myFactionRelation Neutral
			myAlertLevelRanges
			{
				VisionAlertLevelDetectionRange Concerned
				{
					myMaxDistance 9.0
					myAlertLevel Concerned
				}
				VisionAlertLevelDetectionRange Aware
				{
					myMaxDistance 16.0
					myAlertLevel Aware
				}
			}
		}
	}
	myAimedAtAlertLevels
	{
	}
	myShotAtAlertLevel Panicked
	myBullyCooldown 1
	myInheritThreat TRUE
	myInheritThreatDelay 500
}

NonCombatantDetectionProfile DogBooDetectionProfile
{
	myAwareValues
	{
		myThreshold 22.0
		myPauseBeforeValueDecay 3500
		myDecayRate 50.0
	}
	myConcernedValues
	{
		myThreshold 35.0
		myPauseBeforeValueDecay 3000
		myDecayRate 50.0
	}
	myScaredValues
	{
		myThreshold 10.0
		myPauseBeforeValueDecay 18000
	}
	myPanickedValues
	{
		myThreshold 100.0
		myPauseBeforeValueDecay 15000
	}
	myIsBully FALSE
}

