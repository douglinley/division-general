enum NPCGender
{
	None
	Male
	Female
}

//To keep synched with RShared_NPCSpecialRole::Role in the code
enum NPCSpecialRole
{
	None
	Veteran
	Elite
	Named
}

enum NPCOrderType
{
	AutoFireOn
	AutoFireOff
	Follow
	TakeCover
	Move
	MoraleBoost
	Investigate
	FlankTarget
	Advance
	Shoot
	UseSecondarySkill
	HealingAlly
	Aim
}

class AISkillReactionProfile
{
	string mySkillReactionProfileName ""
}

list AISkillReactionProfileList
{
	AISkillReactionProfile
}

class NPCLevelScalingParameters
{
	int myNPCLevelSpanMin < default=1 min=1 max=100 tooltip="Lower Bound for the NPCs Level Span." >
	int myNPCLevelSpanMax < default=100 min=1 max=100 tooltip="Upper Bound for the NPCs Level Span." >
	int myNPCLevelCap < default=0 min=0 max=100 tooltip="Once the NPC has reached the level cap, they will no longer continue to level-up with the player and will always remain at this level. A value of 0 disables the cap." >
}

class NPCTacticalAnalysisSettings
{
	float myVisibilityUpdateDelayInSeconds 1.0
	float myCornerCoverPositionBonus 10.0
	float myOtherFireteamMemberInvestigationPositionAvoidanceRadius 5.0
	float myOtherFireteamMemberInvestigationPositionPenalty 30.0
	float myMaximumAgentSpeed 5.0
	float myInDirectionOfTargetVelocityBonus 20.0
}

class HardModeNPCRoleMapping
{
	NPCSpecialRole myNoneMapping
	NPCSpecialRole myVeteranMapping
	NPCSpecialRole myEliteMapping
}

class ChallengeModeNPCSettings
{
	NPCSpecialRole myMinRole
	int mySpawnLevel < default=32 min=1 max=100 tooltip="The spawn level NPCs in Challenge Mode" >
}

class NPCSettings
{
	float myMeleeDistance 10.0

	float myClaimTimeOut 30.0
	float myContributionTimeOut 60.0

	float myDefaultNPCDespawnTime 10.0
	float mySpawnClosetNPCKillTime 60.0 < tooltip="Time delay in seconds before NPC's stuck in spawn closets die" >
	
	float myRoamerRespawnTime 300.0 < tooltip="Time delay in seconds before the roamer can spawn again" >
	float myRoamerMovementSpeed 1.0 < tooltip="Movement speed in meter per sec when the roamer is inactive not loaded" >
	
	int myMaxShootTicketsFor1 2
	int myMaxShootTicketsFor2 3
	int myMaxShootTicketsFor3 4
	int myMaxShootTicketsFor4 5
	
	int mySimultaneousBarkLimit 3
	float mySimultaneousBarkLimitCooldown 1.5
	
	file myAIDialogDataXML
	file myNPCTicketTypes
	
	int myEliteScalingLevelBonus 2
	
	AISkillReactionProfileList mySkillReactionProfiles
	
	float myLevelAdjustmentModifier < default=0.0 min=0.0 max=10.0 tooltip="Once the player is outside of the level span, the NPCs will follow the player's level, but at a modified rate." >
	int myMaximumLevelDelta < default=5 min=0 max=100 tooltip="NPCs will always stay within a certain delta of the player level." >

	NPCTacticalAnalysisSettings myNPCTacticalAnalysisSettings
	HardModeNPCRoleMapping myHardModeNPCRoleMapping
	ChallengeModeNPCSettings myChallengeModeNPCSettings
}
